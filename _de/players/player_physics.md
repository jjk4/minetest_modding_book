---
title: Spielerphysiken
layout: default
root: ../..
idx: 4.4
description: Erfahren Sie, wie Sie einen Spieler schneller laufen, höher springen oder einfach schweben lassen können.
redirect_from: /de/chapters/player_physics.html
---

## Einleitung <!-- omit in toc -->

Die Spielerphysik kann mit Hilfe von Physik-Overrides verändert werden.
Physik-Overrides können die Gehgeschwindigkeit, die Sprunggeschwindigkeit
und Schwerkraftkonstanten einstellen.
Physiküberschreibungen werden für jeden Spieler einzeln festgelegt
und sind Multiplikatoren.
Ein Wert von 2 für die Schwerkraft würde zum Beispiel die Schwerkraft doppelt so stark machen.

- [Grundlegendes Beispiel](#grundlegendes-beispiel)
- [Verfügbare Overrides](#verfügbare-overrides)
	- [Altes Bewegungsverhalten](#altes-bewegungsverhalten)
- [Mod-Inkompatibilität](#mod-inkompatibilität)
- [Sie sind dran](#sie-sind-dran)

## Grundlegendes Beispiel

Hier ist ein Beispiel für das Hinzufügen eines Antigravitationsbefehls, der
den Aufrufer in eine niedrige Schwerkraft versetzt:

```lua
minetest.register_chatcommand("antigravity", {
    func = function(name, param)
        local player = minetest.get_player_by_name(name)
        player:set_physics_override({
            gravity = 0.1, -- setzt die Schwerkraft auf 10% des ursprünglichen Wertes
                           -- (0.1 * 9.81)
        })
    end,
})
```

## Verfügbare Overrides

`player:set_physics_override()` wird eine Tabelle mit Overrides übergeben.\\
Laut [lua_api.txt](https://minetest.gitlab.io/minetest/class-reference/#player-only-no-op-for-other-objects),
können diese sein:

* speed: Multiplikator zum Standardwert für die Gehgeschwindigkeit (Standard: 1)
* jump: Multiplikator auf Standard-Sprungwert (Standard: 1)
* gravity: Multiplikator zum Standardwert für die Schwerkraft (Standard: 1)
* sneak: ob der Spieler schleichen kann (Standard: true)

### Altes Bewegungsverhalten

Die Spielerbewegung vor der Version 0.4.16 beinhaltete den sneak glitch, der
verschiedene Bewegungs-Glitches erlaubt, darunter die Fähigkeit
einen 'Aufzug' zu erklimmen, der aus einer bestimmten Anordnung von Nodes besteht, indem man sich anschleicht
(Umschalttaste drücken) und die Leertaste drücken, um aufzusteigen.
Obwohl dieses Verhalten nicht beabsichtigt war, wurde es in den Überschreibungen beibehalten, da es auf vielen Servern verwendet wird.

Um das alte Bewegungsverhalten vollständig wiederherzustellen, sind zwei Überschreibungen erforderlich:

* new_move: ob der Spieler neue Bewegungen verwendet (Standard: true)
* sneak_glitch: ob der Spieler 'Schleichfahrstühle' benutzen kann (Standard: false)

## Mod-Inkompatibilität

Bitte beachten Sie, dass Mods, die denselben Physikwert eines Spielers überschreiben, dazu neigen
inkompatibel zueinander zu haben. Wenn ein Override gesetzt wird, überschreibt er
Überschreibungen, die zuvor gesetzt wurden. Das bedeutet, dass wenn mehrere Überschreibungen
die Geschwindigkeit eines Spielers festlegen, ist nur die zuletzt ausgeführte wirksam.

## Sie sind dran

* **Sonic**: Setzen Sie den Geschwindigkeitsmultiplikator auf einen hohen Wert (mindestens 6), wenn ein Spieler dem Spiel beitritt.
* **Super bounce**: Erhöhe den Sprungwert, so dass der Spieler 20 Meter weit springen kann (1 Meter ist 1 Node).
* **Space**: Die Schwerkraft sollte abnehmen, wenn der Spieler höher steigt.
