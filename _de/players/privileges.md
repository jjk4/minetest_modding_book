---
title: Privilegien
layout: default
root: ../..
idx: 4.1
description: Privs registrieren.
redirect_from: /de/chapters/privileges.html
---

## Einleitung <!-- omit in toc -->

Privilegien, oft privs abgekürzt, geben Spielern die Möglichkeit,
bestimmte Aktionen durchzuführen. Serverbesitzer können Privilegien vergeben und entziehen, um zu kontrollieren
welche Fähigkeiten jeder Spieler hat.

- [Wann sollten Sie Privilegien nutzen?](#wann-sollten-sie-privilegien-nutzen)
- [Erklärung von Privilegien](#erklärung-von-privilegien)
- [Überprüfung der Privilegien](#überprüfung-der-privilegien)
- [Abrufen und Festlegen von Privilegien](#abrufen-und-festlegen-von-privilegien)
- [Privilegien zu basic_privs hinzufügen](#privilegien-zu-basicprivs-hinzufügen)

## Wann sollten Sie Privilegien nutzen?

Ein Privileg sollte einem Spieler die Möglichkeit geben, etwas zu tun.
Privilegien sind **nicht** dazu da, Klassen- oder Statusangaben zu machen.

**Gute Privilegien:**

* interact
* shout
* noclip
* fly
* kick
* ban
* vote
* worldedit
* area_admin - Adminfunktionen eines Mods sind in Ordnung

**Schlechte Privilegien:**

* moderator
* admin
* elf
* dwarf

## Erklärung von Privilegien

Verwenden Sie `register_privilege`, um ein neues Privileg zu deklarieren:

```lua
minetest.register_privilege("vote", {
    description = "Kann über Themen abstimmen",
    give_to_singleplayer = true
})
```

`give_to_singleplayer` steht standardmäßig auf true, wenn es nicht angegeben wird, so dass es
in der obigen Definition nicht wirklich benötigt wird.

## Überprüfung der Privilegien

Um schnell zu überprüfen, ob ein Spieler alle erforderlichen Rechte besitzt:

```lua
local has, missing = minetest.check_player_privs(player_or_name,  {
    interact = true,
    vote = true })
```

In diesem Beispiel ist `has` wahr, wenn der Spieler alle benötigten Privilegien hat.
Wenn `has` falsch ist, dann enthält `missing` eine Schlüssel-Wert-Tabelle
mit den fehlenden Privilegien.

```lua
local has, missing = minetest.check_player_privs(name, {
        interact = true,
        vote = true })

if has then
    print("Spieler hat alle Privilegien!")
else
    print("Dem Spieler fehlen folgende Privilegien:" .. dump(missing))
end
```

Wenn Sie die fehlenden Privilegien nicht überprüfen müssen, können Sie
check_player_privs" direkt in die if-Anweisung einfügen.

```lua
if not minetest.check_player_privs(name, { interact=true }) then
    return false, "Hierfür brauchen Sie Interaktion!"
end
```

## Abrufen und Festlegen von Privilegien

Auf die Spielerprivilegien kann unabhängig, ob der Spieler
online ist, zugegriffen werden.


```lua
local privs = minetest.get_player_privs(name)
print(dump(privs))

privs.vote = true
minetest.set_player_privs(name, privs)
```

Privilegien werden immer als Schlüssel-Wert-Tabelle angegeben, wobei der Schlüssel der
Name der Berechtigung und der Wert ein Boolescher Wert ist.

```lua
{
    fly = true,
    interact = true,
    shout = true
}
```

## Privilegien zu basic_privs hinzufügen

Spieler mit dem Privileg `basic_privs` können eine begrenzte Anzahl von Privilegien gewähren und entziehen.
Es ist üblich, dieses Privileg an Moderatoren zu vergeben, so dass
sie `interact` und `shout` gewähren und entziehen können, aber nicht sich selbst oder anderen
Spieler Privilegien mit größerem Missbrauchspotential wie `give` und `server` gewähren können.

Um ein Privileg zu `basic_privs` hinzuzufügen, und um einzustellen, welche Privilegien Ihre Moderatoren
anderen Spielern gewähren und entziehen können, müssen Sie die Einstellung `basic_privs` ändern.

Standardmäßig hat `basic_privs` den folgenden Wert:

    basic_privs = interact, shout

Um `vote` hinzuzufügen, aktualisieren Sie dies zu:

    basic_privs = interact, shout, vote

Dies wird es Spielern mit `basic_privs` erlauben, das `vote` Privileg zu gewähren und zu entziehen.
