---
title: Chat und Befehle
layout: default
root: ../..
idx: 4.2
description: Registrierung eines Chatbefehls und Behandlung von Chatnachrichten mit register_on_chat_message
redirect_from: /de/chapters/chat.html
cmd_online:
    level: warning
    title: Offline-Spieler können Befehle ausführen
    message: <p>Ein Spielername wird anstelle eines Spielerobjekts übergeben, da Mods
             Befehle im Namen von Offline-Spielern ausführen können. Zum Beispiel erlaubt die IRC-
             Brücke den Spielern, Befehle auszuführen, ohne dem Spiel beizutreten.</p>

             <p>Stellen Sie also sicher, dass Sie nicht davon ausgehen, dass der Spieler online ist.
             Du kannst überprüfen, ob <pre>minetest.get_player_by_name</pre> einen Spieler liefert.</p>

cb_cmdsprivs:
    level: warning
    title: Privilegien und Chat-Befehle
    message: Das "Shout"-Privileg ist für einen Spieler nicht erforderlich, um diesen Callback auszulösen.
             Das liegt daran, dass Chat-Befehle in Lua implementiert sind, und nur
             Chat-Nachrichten sind, die mit einem / beginnen.

---

## Einleitung <!-- omit in toc -->

Mods können mit dem Spielerchat interagieren, einschließlich
Senden von Nachrichten, Abfangen von Nachrichten und Registrieren von Chat-Befehlen.

- [Senden von Nachrichten an alle Spieler](#senden-von-nachrichten-an-alle-spieler)
- [Nachrichten an bestimmte Spieler senden](#nachrichten-an-bestimmte-spieler-senden)
- [Chat-Befehle](#chat-befehle)
- [Komplexe Unterbefehle](#komplexe-unterbefehle)
- [Abfangen von Nachrichten](#abfangen-von-nachrichten)

## Senden von Nachrichten an alle Spieler

Um eine Nachricht an alle Spieler im Spiel zu senden, rufe die Funktion chat_send_all auf.

```lua
minetest.chat_send_all("Dies ist eine Chat-Nachricht an alle Spieler")
```

Hier ist ein Beispiel dafür, wie dies im Spiel aussieht:

    <player1> Sehen Sie sich diesen Eingang an
    Dies ist eine Chat-Nachricht an alle Spieler
    <player2> Was ist damit?

Die Nachricht erscheint in einer separaten Zeile, um sie vom Spieler-Chat im Spiel zu unterscheiden.

## Nachrichten an bestimmte Spieler senden

Um eine Nachricht an einen bestimmten Spieler zu senden, rufen Sie die Funktion chat_send_player auf:

```lua
minetest.chat_send_player("Spieler1", "Dies ist eine Chat-Nachricht für Spieler1")
```

Diese Nachricht wird auf dieselbe Weise angezeigt wie die Nachrichten an alle Spieler, ist aber
nur für den benannten Spieler sichtbar, in diesem Fall für Spieler1.

## Chat-Befehle

Um einen Chat-Befehl zu registrieren, zum Beispiel `/foo`, verwenden Sie `register_chatcommand`:

```lua
minetest.register_chatcommand("foo", {
    privs = {
        interact = true,
    },
    func = function(name, param)
        return true, "Sie sagten " .. param .. "!"
    end,
})
```

Im obigen Ausschnitt ist "interact" als erforderliches
[Privileg](privileges.html) aufgeführt, was bedeutet, dass nur Spieler mit dem Privileg "interact" den Befehl ausführen können.

Chat-Befehle können bis zu zwei Werte zurückgeben,
Der erste ist ein boolescher Wert, der den Erfolg anzeigt, und der zweite ist eine
Nachricht, die an den Benutzer gesendet wird.

{% include notice.html notice=page.cmd_online %}

## Komplexe Unterbefehle

Es wird oft benötigt, komplexe Chat-Befehle zu bereitzustellen, wie z.B.:

* `/msg <zu> <Nachricht>`
* `/team join <Teamname>`
* `/team leave <Teamname>`
* `/team list`

Dies geschieht normalerweise mit [Lua-Mustern] (https://www.lua.org/pil/20.2.html).
Patterns sind eine Methode, um anhand von Regeln Dinge aus Text zu extrahieren.

```lua
local zu, msg = string.match(param, "^([%a%d_-]+) (*+)$")
```

Der obige Code implementiert `/msg <zu> <Nachricht>`. Lassen Sie uns von links nach rechts vorgehen:

* `^` bedeutet, dass der Anfang der Zeichenkette übereinstimmt.
* `()` ist eine übereinstimmende Gruppe - alles, was hier drin steht, wird
  von string.match zurückgegeben.
* `[]` bedeutet, dass die Zeichen in dieser Liste akzeptiert werden.
* `%a` bedeutet, jeden Buchstaben zu akzeptieren und `%d` bedeutet, eine beliebige Ziffer zu akzeptieren.
* `[%a%d_-]` bedeutet, einen beliebigen Buchstaben, eine beliebige Ziffer, `_` oder `-` zu akzeptieren.
* `+` bedeutet, dass die Sache ein oder mehrere Male übereinstimmt.
* `*` bedeutet, dass ein beliebiges Zeichen in diesem Zusammenhang übereinstimmt.
* `$` bedeutet, das Ende der Zeichenkette zu finden.

Einfach ausgedrückt: Das Muster entspricht dem Namen (ein Wort mit nur Buchstaben/Zahlen/-/_),
dann ein Leerzeichen, dann die Nachricht (ein oder mehrere beliebige Zeichen). Der Name und die
werden zurückgegeben, da sie von Klammern umgeben sind.

Das ist die Art und Weise, wie die meisten Mods komplexe Chat-Befehle implementieren. Eine bessere Anleitung für Lua
Patterns wäre wahrscheinlich das
[lua-users.org tutorial](http://lua-users.org/wiki/PatternsTutorial)
oder die [PIL-Dokumentation](https://www.lua.org/pil/20.2.html).

<p class="book_hide">
    Es gibt auch eine vom Autor dieses Buches geschriebene Bibliothek, mit der man benutzen
    kann um komplexe Chat-Befehle ohne Muster zu erstellen, den
    <a href="https://gitlab.com/rubenwardy/ChatCmdBuilder">Chat Command Builder</a>.
</p>


## Abfangen von Nachrichten

Um eine Nachricht abzufangen, verwenden Sie register_on_chat_message:

```lua
minetest.register_on_chat_message(function(name, message)
    print(name .. " sagte " .. message)
    return false
end)
```

Wenn Sie false zurückgeben, erlauben Sie, dass die Chat-Nachricht vom Standard
Handler gesendet wird. Sie können die Zeile `return false` sogar entfernen und es würde immer noch
funktionieren, da `nil` implizit zurückgegeben wird und wie false behandelt wird.

{% include notice.html notice=page.cb_cmdsprivs %}

Sie sollten berücksichtigen, dass es sich um einen Chat-Befehl handeln könnte,
oder der Benutzer vielleicht kein `shout` hat.

```lua
minetest.register_on_chat_message(function(name, message)
    if message:sub(1, 1) == "/" then
        print(name .. " hat einen Chat-Befehl ausgeführt")
    elseif minetest.check_player_privs(name, { shout = true }) then
        print(name .. " sagte " .. message)
    else
        print(name .. " versucht zu sagen " .. message ..
                " hat aber kein shout")
    end

    return false
end)
```
