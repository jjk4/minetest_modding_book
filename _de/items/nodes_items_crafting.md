	---
title: Nodes, Items und Craften
layout: default
root: ../..
idx: 2.1
description: Benutze register_node, register_item und register_craft um zu lernen, wie man Nodes, Items und Rezepte erstellt.
redirect_from: /de/chapters/nodes_items_crafting.html
---

## Einführung <!-- omit in toc -->

Neue Nodes, Craftitems und Rezepte zu erstellen 
sind Grundlagen von vielen Mods

- [Was sind Nodes und Craftitems?](#was-sind-nodes-und-craftitems)
- [Items erstellen](#items-erstellen)
    - [Itemnamen](#itemnamen)
    - [Itemaliase](#itemaliase)
    - [Texturen](#texturen)
- [Erstellen von Basis-Nodes](#erstellen-von-basis-nodes)
- [Crafting](#crafting)
  - [Shaped](#shaped)
  - [Shapeless](#shapeless)
  - [Cooking und Fuel](#cooking-und-fuel)
- [Gruppen](#gruppen)
- [Werkzeuge, Fähigkeiten und Grabungstypen](#werkzeuge-fähigkeiten-und-grabungstypen)

## Was sind Nodes und Craftitems?

Nodes, Craftitems und Tools sind alles Items. Ein Item ist etwas, das im Inventar gefunden werden kann - sogar wenn es im normalen Gameplay nicht möglich ist.

Ein Node ist ein Item, das platziert oder in der Welt gefunden werden kann. Jede Position muss belegt werden mit einem und nur einem Node - scheinbar leere Position sind normalerweise Luftblöcke.

Ein Craftitem kann nicht platziert werden. Man kann es nur im Inventar finden oder als gedropptes Item in der Welt.

Ein Werkzeug hat die Fähigkeit sich abzunutzen und hat normalerweise nicht standardmäßige Abbaufähigkeiten. In Zukunft werden Craftitems und Werkzeuge wahrscheinlich verschmelzen, weil die Unterscheidung zwischen ihnen eher ausgedacht ist.

## Items erstellen

Itemdefinitionen bestehen aus einen *Itemnamen* und einer *Definitionstabelle*. Die Definitionstabellen beinhalten Attribute, welche das Verhalten eines Items beeinflussen.

```lua
minetest.register_craftitem("modname:itemname", {
    description = "Mein spezielles Item",
    inventory_image = "modname_itemname.png"
})
```

### Itemnamen

jedes Item hat einen Itemnamen, welches auf sich verweist. Er sollte folgendes Format haben:

    modname:itemname

### Itemaliase
Items können auch *Aliase* haben, die auf ihren Namen zeigen. Ein *Alias* ist ein nachgemachter Itemname, der dazu führt, dass die Engine alle Aliase so behandelt als wären es Itemnamen. Es gibt hierfür zwei verbreitete Anwendungsmöglichkeiten:

* Umbenennen von entfernten Items. Es kann unbekannte Items in der Welt oder im Inventar geben, wenn ein Gegenstand ohne Korrektur aus einen Mod entfernt wird.
* Hinzufügen von Abkürzungen. `/giveme dirt` ist einfacher als `/giveme default:dirt`.

Ein Itemalias zu erstellen ist richtig einfach. Ein guter Weg um sich die Reinfolge von der Argumenten zu merken ist `von → zu` wo *von* der alias ist und *zu* das Orginal.

```lua
minetest.register_alias("dirt", "default:dirt")
```

Mods müssen sicher gehen, dass Alias aufgelöst werden, bevor sie sich direkt mit Itemnamen befassen, da die Engine dies nicht tut. Das ist allerdings ziemlich einfach:

```lua
itemname = minetest.registered_aliases[itemname] or itemname
```


### Texturen

Texturen solten in den textures/ Pfad mit Namen im Format 
`modname_itemname.png`.\\
platziert werden. JPEG Texturen werden unterstüzt, aber sie unterstützen keine Transparenz und haben generell
schlechte Qualität und eine niedrige Auflösung.
Es ist oft besser das PNG Format zu benutzen.

Texturen in Minetest sind in der Regel 16 mal 16 Pixel.
Sie können in jeder Auflösung sein, es wird jedoch empfohlen, dass sie in der Größenordnung von 2 liegen, beispielsweise 16, 32, 64 oder 128.
Das liegt daran, dass andere Auflösungen auf älteren Geräten möglicherweise nicht korrekt unterstützt werden, was zu einer geringeren Leistung führt.

## Erstellen von Basis-Nodes
```lua
minetest.register_node("meinemod:diamant", {
    description = "Alien Diamanten",
    tiles = {"meinemod_diamant.png"},
    is_ground_content = true,
    groups = {cracky=3, stone=1}
})
```
Die Eigenschaft `tiles` ist eine Tabelle mit Texturnamen, die der Node verwenden wird.
Wenn es nur eine Textur gibt, wird diese Textur auf jeder Seite verwendet.
Um eine andere Textur pro Seite zu erhalten, geben Sie die Namen von 6 Texturen in dieser Reihenfolge an:

    oben (+Y), unten (-Y), rechts (+X), links (-X), hinten (+Z), vorne (-Z).
    (+Y, -Y, +X, -X, +Z, -Z)

Denken Sie daran, dass +Y in Minetest nach oben zeigt, wie es in den meisten 3D-Computerspielen der Fall ist.

```lua
minetest.register_node("meinemod:diamant", {
    description = "Alien Diamanten",
    tiles = {
        "meinemod_diamant_oben.png",    -- y+
        "meinemod_diamant_unten.png",  -- y-
        "meinemod_diamant_rechts.png", -- x+
        "meinemod_diamant_links.png",  -- x-
        "meinemod_diamant_hinten.png",  -- z+
        "meinemod_diamant_vorne.png", -- z-
    },
    is_ground_content = true,
    groups = {cracky = 3},
    drop = "meinemod:diamant_fragment"
    -- ^  Anstatt diamanten dropen zu lassen, lassen Sie meinemod:diamant_fragment dropen
})
```

Mit dem Attribut `is_ground_content` können Höhlen über dem Stein erzeugt werden. Dies ist wichtig für jeden Node, der während der Kartenerstellung unterirdisch platziert werden kann. Höhlen werden aus der Welt herausgeschnitten, nachdem alle anderen Nodes in einem Gebiet generiert wurden.

## Crafting

Es gibt verschiedene Arten von Craftingrezepten, die durch die Eigenschaft `type` angezeigt werden.

* shaped - Die Zutaten müssen sich an der richtigen Stelle befinden.
* shapeless - Es spielt keine Rolle, wo sich die Zutaten befinden,
  es muss nur die richtige Menge vorhanden sein.
* cooking - Rezepte, die der Ofen verwenden soll.
* fuel - Definiert Gegenstände, die in Öfen verbrannt werden können.
* tool_repair - Definiert Gegenstände, die mit Werkzeugen repariert werden können.

Craftingrezepte sind keine Gegenstände, daher verwenden sie keine eindeutigen Itemnamen um sie zu identifizieren.

### Shaped

Shaped Rezepte sind Rezepte, bei denen die Zutaten die richtige Form oder das richtige
Muster haben müssen, um zu funktionieren. In dem folgenden Beispiel müssen die Fragmente in einem
stuhlähnlichen Muster liegen, damit das Crafting funktioniert.

```lua
minetest.register_craft({
    type = "shaped",
    output = "meinemod:diamant_stuhl 99",
    recipe = {
        {"meinemod:diamant_fragment", "",                           ""},
        {"meinemod:diamant_fragment", "meinemod:diamant_fragment",  ""},
        {"meinemod:diamant_fragment", "meinemod:diamant_fragment",  ""}
    }
})
```

Zu beachten ist auch die leere Spalte auf der rechten Seite.
Das bedeutet, dass rechts von der Form eine leere Spalte vorhanden sein *muss*, sonst
wird dies nicht funktionieren.
Wenn diese leere Spalte nicht erforderlich sein sollte, können die leeren Strings
folgendermaßen weggelassen werden:

```lua
minetest.register_craft({
    type = "shaped",
    output = "meinemod:diamant_stuhl 99",
    recipe = {
        {"meinemod:diamant_fragment", ""                         },
        {"meinemod:diamant_fragment", "meinemod:diamant_fragment"},
        {"meinemod:diamant_fragment", "meinemod:diamant_fragment"}
    }
})
```

Das Feld "type" wird für Shaped Rezepte nicht benötigt, da "shaped" der
Standard-Craftingtyp ist.

### Shapeless

Shapeless Rezepte sind eine Art von Rezepten, bei denen es nicht darauf ankommt
wo die Zutaten platziert werden, sondern nur, dass sie da sind.

```lua
minetest.register_craft({
    type = "shapeless",
    output = "meinemod:diamant 3",
    recipe = {
        "meinemod:diamant_fragment",
        "meinemod:diamant_fragment",
        "meinemod:diamant_fragment",
    },
})
```

### Cooking und Fuel

Rezepte des Typs "cooking" werden nicht im crafting grid hergestellt,
sondern im Ofen oder anderen Kochwerkzeugen, welche womöglich in Mods gefunden werden können, gekocht


```lua
minetest.register_craft({
    type = "cooking",
    output = "meinemod:diamant_fragment",
    recipe = "default:coalblock",
    cooktime = 10,
})
```

Der einzige wirklich Unterschied im Code ist, dass das Rezept nur ein Item 
im Vergleich zu einer Tabelle (zwischen geschweiften Klammern) beinhaltet.
Sie haben optional auch einen "cooktime" Parameter, welcher
definiert wie lange die Items zu kochen brauchen.
Wenn dies nicht gesetzt ist, ist der Standard 3.

Das Rezept oben funktioniert, wenn der Kohleblock im Input-Slot ist,
mit irgendeiner Form von Brennstoff darunter.
Es stellt nach 10 Sekunden ein Diamant-Fragment her!

Dieser Typ ist eine Ergänzung zum Kochtyp, da er definiert,
was in Öfen und anderen Kochgeräten aus Mods verbrannt werden kann.

```lua
minetest.register_craft({
    type = "fuel",
    recipe = "meinemod:diamant",
    burntime = 300,
})

Sie haben keinen Ausgabe wie andere Rezepte, aber sie haben eine Brenndauer
die angibt, wie lange sie in Sekunden als Brennstoff reichen.
Der Diamant ist also 300 Sekunden lang als Brennstoff verwendbar!

## Gruppen

Items können Mitglieder von vielen Gruppen sein und Gruppen können viele Mitglieder haben.
Gruppen werden durch die Verwendung der `groups`-Eigenschaft in der Definitions-Tabelle definiert
und haben einen dazugehörigen Wert.

```lua
groups = {cracky = 3, wood = 1}
```

Es gibt mehrere Gründe für die Verwendung von Gruppen.
Erstens werden Gruppen verwendet, um Eigenschaften wie Abbautyp und Entflammbarkeit zu beschreiben.
Zweitens können Gruppen in einem Handwerksrezept anstelle eines Itemnamens verwendet werden, damit
ein beliebiges Item aus der Gruppe verwendet werden kann.

```lua
minetest.register_craft({
    type = "shapeless",
    output = "meinemod:diamant_ding 3",
    recipe = {"group:holz", "mymod:diamant"}
})
```

## Werkzeuge, Fähigkeiten und Grabungstypen

Dig types are groups which are used to define how strong a node is when dug
with different tools.


Grabungstypen sind Gruppen, die dazu benutzt werden, um zu definieren wie stark ein Node ist, wenn er
von verschiedenen Werkzeugen abgebaut wird.
Eine Grabtypgruppe mit einem höheren Wert bedeutet, dass der Node leichter
und schneller abzubauen ist.
Es ist möglich, mehrere Grabtypen zu kombinieren, um eine effizientere Nutzung
von mehreren Werkzeugtypen zu erzielen.
Ein Node ohne Grabtypen kann mit keinem Werkzeug abgebaut werden.


| Gruppe  | Bestes Werkzeug | Beschreibung |
|---------|-----------------|-------------|
| crumbly | Schaufel    | Erde, Sand |
| cracky | Spitzhacke   | Zähes (aber brüchiges) Material wie Stein |
| snappy | *irgendeins*       | Kann mit feinen Werkzeugen geschnitten werden;<br>z.B. Blätter, kleine Pflanzen, Draht, Metallbleche  |
| choppy | Axt | Kann mit scharfer Gewalt geschnitten werden, z. B. Bäume, Holzbretter  |
| fleshy | Schwert | Lebende Dinge wie Tiere und der Spieler <br>Das könnte einige Bluteffekte beim Schlagen mit sich bringen. |
| explody | ? | Besonders anfällig für Explosionen  |
| oddly_breakable_by_hand | *irgendeins* | Fackeln und dergleichen - sehr schnell zu graben |


Jedes Werkzeug hat eine Werkzeugfähigkeit.
Eine Fähigkeit umfasst eine Liste der unterstützten Grabtypen und die zugehörigen Eigenschaften
für jeden Typ, wie z. B. die Grabungszeiten und der Grad der Abnutzung.
Werkzeuge können auch eine maximal unterstützte Härte für jeden Typ haben, was es ermöglicht
schwächere Werkzeuge daran zu hindern, härtere Nodes zu graben.
Es ist sehr üblich, dass Werkzeuge alle Grabtypen in ihren Fähigkeiten enthalten.
Die weniger geeigneten haben dabei sehr ineffiziente Eigenschaften.
Wenn der Gegenstand, den ein Spieler gerade trägt, keine explizite Werkzeugfähigkeit 
hat, dann wird stattdessen die Fähigkeit der aktuellen Hand verwendet.

```lua
minetest.register_tool("meinemod:werkzeug", {
    description = "Mein Werkzeug",
    inventory_image = "meinemod_werkzeug.png",
    tool_capabilities = {
        full_punch_interval = 1.5,
        max_drop_level = 1,
        groupcaps = {
            crumbly = {
                maxlevel = 2,
                uses = 20,
                times = { [1]=1.60, [2]=1.20, [3]=0.80 }
            },
        },
        damage_groups = {fleshy=2},
    },
})
```

Groupcaps ist die Liste der unterstützten Grabungstypen für Grabungsnodes.
Schadensgruppen (Damage Groups) dienen zur Steuerung der Art und Weise, wie Werkzeuge Objekte beschädigen; dies wird
später im Kapitel Objekte, Spieler und Entities behandelt werden.
