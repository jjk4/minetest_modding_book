---
title: Node Zeichnungstypen
layout: default
root: ../..
idx: 2.3
description: Ratgeber für alle Zeichnungstypen, einschließlich node boxen/Nodeboxen und mesh nodes.
redirect_from: /de/chapters/node_drawtypes.html
---

## Einleitung <!-- omit in toc -->

Die Methode, mit der ein Node gezeichnet wird, wird als *Zeichnungstyp* bezeichnet. Es gibt viele
verfügbare Zeichnungstypen. Das Verhalten eines Zeichnungstyps kann
durch Angabe von Eigenschaften in der Nodetypdefinition gesteuert werden. Diese Eigenschaften
sind für alle Instanzen dieses Nodes festgelegt. Es ist möglich, einige Eigenschaften
pro Node zu steuern, indem man `param2` verwendet.

Im vorigen Kapitel wurde das Konzept der Nodes und Items eingeführt, aber eine
vollständige Definition eines Nodes wurde nicht gegeben. Die Minetest-Welt ist ein 3D-Gitter aus
Positionen. Jede Position wird als Node bezeichnet und besteht aus dem Nodetyp
(Name) und zwei Parametern (param1 und param2). Die Funktion
`minetest.register_node` (übersetzt `minetest.registriere_node`)ist ein wenig irreführend, da sie nicht wirklich
einen Node registriert - sie registriert einen neuen *Typ* von Node.

Die Nodeparameter werden zum steuern, wie ein Nde individuell gerendert wird, verwendet.
`param1` wird verwendet, um die Beleuchtung eines Nodes zu speichern, und die Bedeutung von
`param2` hängt von der Eigenschaft `paramtype2` der Nodetypdefinition ab.

- [Würfelförmiger Node: Normale und allfaces](#würfelförmiger-node-normale-und-allfaces)
- [Glasartige Nodes](#glasartige-nodes)
	- [Glasartig gerahmt](#glasartig-gerahmt)
- [Airlike Nodes](#airlike-nodes)
- [Lighting and Sunlight Propagation](#lighting-and-sunlight-propagation)
- [Flüssige Nodes](#flüssige-nodes)
- [Nodeboxen](#nodeboxen)
	- [Wandgehaltene Nodeboxen](#wandgehaltene-nodeboxen)
- [Mesh Nodes](#mesh-nodes)
- [Signlike Nodes](#signlike-nodes)
- [Plantlike Nodes](#plantlike-nodes)
- [Firelike Nodes](#firelike-nodes)
- [Mehr Zeichnungstypen](#mehr-zeichnungstypen)


## Würfelförmiger Node: Normale und allfaces

<figure class="right_image">
    <img src="{{ page.root }}//static/drawtype_normal.png" alt="Normaler Zeichnungstyp">
    <figcaption>
        Normaler Zeichnungstyp
    </figcaption>
</figure>

Der Zeichnungstyp normal wird typischerweise zum Rendern eines Würfelförmigen Nodes verwendet.
Wenn die Seite eines normalen Nodes an einer festen Seite anliegt, wird diese Seite nicht gerendert,
was zu einem großen Leistungsgewinn führt.

Im Gegensatz dazu wird der Zeichnungstyp allfaces immer noch die Innenseite darstellen, wenn er auf einen
soliden Node ist. Dies ist gut für Nodes mit teilweise transparenten Seiten, wie z.B. den
Blatt-Node. Sie können den allfaces_optional drawtype verwenden, um den Benutzern die Möglichkeit zu geben, die
langsamere Zeichnen auszuschalten, in diesem Fall verhält er sich wie ein normaler Node.

```lua
minetest.register_node("meinemod:diamant", {
    description = "Alien Diamant",
    tiles = {"meinemod_diamant.png"},
    groups = {cracky = 3},
})

minetest.register_node("default:leaves", {
    description = "Blätter",
    drawtype = "allfaces_optional",
    tiles = {"default_leaves.png"}
})
```

Hinweis: Der normale Zeichentyp ist der Standard-Zeichentyp, Sie müssen ihn also nicht explizit
angeben.


## Glasartige Nodes

Der Unterschied zwischen glasartigen(glasslike) und normalen Nodes besteht darin, dass die Platzierung eines glasartigen Nodes
neben einem normalen Node, nicht dazu führt, dass die Seite des normalen Nodes ausgeblendet wird.
Dies ist nützlich, weil glasartige Nodes in der Regel transparent sind und daher die Verwendung eines normalen
Drawtype dazu führen würde, dass man durch die Welt hindurchsehen kann.

<figure>
    <img src="{{ page.root }}//static/drawtype_glasslike_edges.png" alt="Glasartige Ränder">
    <figcaption>
        Glasartige Ränder
    </figcaption>
</figure>

```lua
minetest.register_node("default:obsidian_glass", {
    description = "Obsidian Glas",
    drawtype = "glasslike",
    tiles = {"default_obsidian_glass.png"},
    paramtype = "light",
    is_ground_content = false,
    sunlight_propagates = true,
    sounds = default.node_sound_glass_defaults(),
    groups = {cracky=3,oddly_breakable_by_hand=3},
})
```

### Glasartig gerahmt

Dies führt dazu, dass die Kante des Nodes mit einem 3D-Effekt um das ganze Ding herumgeht und nicht
um einen einzelnen Node, wie im Folgenden dargestellt:

<figure>
    <img src="{{ page.root }}//static/drawtype_glasslike_framed.png" alt="Glasartig gerahmt Ränder">
    <figcaption>
        Glasartig gerahmt Ränder
    </figcaption>
</figure>

Sie können den Zeichnungstyp `glasslike_framed_optional` verwenden, um dem Benutzer die Möglichkeit zu geben,
das gerahmte Erscheinungsbild manuell zu aktivieren.

```lua
minetest.register_node("default:glass", {
    description = "Glas",
    drawtype = "glasslike_framed",
    tiles = {"default_glass.png", "default_glass_detail.png"},
    inventory_image = minetest.inventorycube("default_glass.png"),
    paramtype = "light",
    sunlight_propagates = true, -- Sonnenlicht kann durch den Node scheinen
    groups = {cracky = 3, oddly_breakable_by_hand = 3},
    sounds = default.node_sound_glass_defaults()
})
```


## Luftartige Nodes

Diese Nodes werden nicht gerendert und haben daher keine Texturen.

```lua
minetest.register_node("meineluft:luft", {
    description = "MeineLuft (Du hacker du!)",
    drawtype = "airlike",
    paramtype = "light",
    sunlight_propagates = true,

    walkable     = false, -- Würde den Spieler mit dem Luftnode kollidieren lassen
    pointable    = false, -- Sie können den Node nicht auswählen
    diggable     = false, -- Sie können den Node nicht abbauen
    buildable_to = true,  -- Nodes können diesen Node ersetzen
                          -- (Sie können einen Node platzieren und den Luftnode 
                          -- entfernen die früher einmal da waren)

    air_equivalent = true,
    drop = "",
    groups = {not_in_creative_inventory=1}
})
```


## Beleuchtung und Sonnenlichtausbreitung

Die Beleuchtung eines Nodes wird in param1 gespeichert. Um herauszufinden, wie man die
Seite eines Nodes zu schattieren hat, wird der Lichtwert des benachbarten Nodes verwendet.
Aus diesem Grund haben solide Nodes keine Lichtwerte, da sie das Licht blockieren.

Standardmäßig lässt ein Nodetyp nicht zu, dass Licht in Nodeinstanzen gespeichert wird.
Es ist normalerweise wünschenswert, dass einige Nodes wie Glas und Luft in der Lage sind,
Licht durchzulassen. Zu diesem Zweck müssen zwei Eigenschaften definiert werden:

```lua
paramtype = "light",
sunlight_propagates = true,
```

Die erste Zeile bedeutet, dass param1 in der Tat den Lichtwert speichert.
Die zweite Zeile bedeutet, dass das Sonnenlicht diesen Node durchlaufen sollte, ohne an Wert zu verlieren.

## Flüssige Nodes

<figure class="right_image">
    <img src="{{ page.root }}//static/drawtype_liquid.png" alt="Flüssigkeiten Zeichnungstyp">
    <figcaption>
        Flüssigkeiten Zeichnungstyp
    </figcaption>
</figure>

Jede Art von Flüssigkeit erfordert zwei Nodedefinitionen - eine für die Flüssigkeitsquelle und
eine weitere für die fließende Flüssigkeit.

```lua
-- Einige Eigenschaften wurden entfernt, da sie nicht mehr
-- den Anwendungsbereich dieses Kapitels entsprechen.
minetest.register_node("default:water_source", {
    drawtype = "liquid",
    paramtype = "light",

    inventory_image = minetest.inventorycube("default_water.png"),
    -- ^ dies ist erforderlich, damit das Inventarbild nicht auch animiert wird

    tiles = {
        {
            name = "default_water_source_animated.png",
            animation = {
                type     = "vertical_frames",
                aspect_w = 16,
                aspect_h = 16,
                length   = 2.0
            }
        }
    },

    special_tiles = {
        -- Wasserquellenmaterial neuen Typs (größtenteils unbenutzt)
        {
            name      = "default_water_source_animated.png",
            animation = {type = "vertical_frames", aspect_w = 16,
                aspect_h = 16, length = 2.0},
            backface_culling = false,
        }
    },

    --
    -- Verhalten
    --
    walkable     = false, -- Der Spieler fällt durch
    pointable    = false, -- Der Spieler kann es nicht auswählen
    diggable     = false, -- Der Spieler kann es nicht abbauen
    buildable_to = true,  -- Nodes können diesen Node ersetzen

    alpha = 160,

    --
    -- Flüssigkeits Eigenschaften
    --
    drowning = 1,
    liquidtype = "source",

    liquid_alternative_flowing = "default:water_flowing",
    -- ^ wenn die Flüssigkeit fließt

    liquid_alternative_source = "default:water_source",
    -- ^ wenn die Flüssigkeit eine Quelle ist

    liquid_viscosity = WATER_VISC,
    -- ^ wie schnell

    liquid_range = 8,
    -- ^ wie weit

    post_effect_color = {a=64, r=100, g=100, b=200},
    -- ^ Farbe des Bildschirms, wenn der Spieler untergetaucht ist
})
```

Fließende Nodes haben eine ähnliche Definition, allerdings mit einem anderen Namen und einer anderen Animation.
Siehe `default:water_flowing` in der Standard-Mod in `minetest_game` für ein vollständiges Beispiel.

## Nodeboxen

<figure class="right_image">
    <img src="{{ page.root }}//static/drawtype_nodebox.gif" alt="Nodebox Zeichnungstyp">
    <figcaption>
        Nodebox Zeichnungstyp
    </figcaption>
</figure>

Mit Nodeboxen können Sie einen Node erstellen, der nicht würfelförmig ist, sondern
aus beliebig vielen Quadern besteht.

```lua
minetest.register_node("stairs:stair_stone", {
    drawtype = "nodebox",
    paramtype = "light",
    node_box = {
        type = "fixed",
        fixed = {
            {-0.5, -0.5, -0.5, 0.5, 0, 0.5},
            {-0.5, 0, 0, 0.5, 0.5, 0.5},
        },
    }
})
```

Der wichtigste Teil ist die Nodebox-Tabelle:

```lua
{-0.5, -0.5, -0.5,       0.5,    0,  0.5},
{-0.5,    0,    0,       0.5,  0.5,  0.5}
```

Jede Zeile ist ein Quader, der zu einem einzigen Nodepunkt verbunden ist.
Die ersten drei Zahlen sind die Koordinaten (von -0,5 bis einschließlich 0,5) der
der unteren, vorderen, linken Ecke, die letzten drei Zahlen sind die gegenüberliegende Ecke.
Sie haben die Form X, Y, Z, wobei Y für oben steht.

Sie können den [NodeBoxEditor](https://forum.minetest.net/viewtopic.php?f=14&t=2840) benutzen um
Nodeboxen durch Ziehen der Kanten zu erstellen. Das ist anschaulicher als die Arbeit von Hand.


### Wandgehaltene Nodeboxen

Manchmal möchte man verschiedene Nodeboxen für die Platzierung auf dem Boden, an der Wand oder an der Decke wie bei Fackeln.

```lua
minetest.register_node("default:sign_wall", {
    drawtype = "nodebox",
    node_box = {
        type = "wallmounted",

        -- Decke
        wall_top    = {
            {-0.4375, 0.4375, -0.3125, 0.4375, 0.5, 0.3125}
        },

        -- Boden
        wall_bottom = {
            {-0.4375, -0.5, -0.3125, 0.4375, -0.4375, 0.3125}
        },

        -- Wand
        wall_side   = {
            {-0.5, -0.3125, -0.4375, -0.4375, 0.3125, 0.4375}
        }
    },
})
```

## Mesh Nodes

Nodeboxen sind zwar im Allgemeinen einfacher zu erstellen, doch sind sie insofern eingeschränkt, als dass
sie nur aus Quadern bestehen können. Nodeboxen sind außerdem nicht optimiert;
Innenflächen werden auch dann noch gerendert, wenn sie vollständig ausgeblendet sind.

Eine Fläche ist eine ebene Oberfläche in einem Mesh(Netz). Eine innere Fläche entsteht, wenn sich die Flächen von zwei
verschiedenen Nodeboxen überlappen, wodurch Teile des Nodebox-Modells
unsichtbar sind, aber dennoch gerendert werden.

So können Sie ein Mesh Node registrieren:

```lua
minetest.register_node("mymod:meshy", {
    drawtype = "mesh",

    -- Enthält die Textur für jedes "material"
    tiles = {
        "mymod_meshy.png"
    },

    -- Verzeichnis zum Mesh
    mesh = "mymod_meshy.b3d",
})
```

Vergewissern Sie sich, dass das Mesh in einem `models`-Verzeichnis verfügbar ist.
Meistens sollte sich das Mesh im eigenen Mod-Ordner befinden, aber es ist auch in Ordnung, wenn
ein Mesh zu verwenden, das von einer anderen Mod bereitgestellt wird, von dem sie abhängig ist. Zum Beispiel kann eine Mod, die
weitere Möbeltypen hinzufügt, das Modell einer grundlegenden
Möbel-Mod sein.


## Signlike Nodes

Signlike Nodes  sind flache Nodes, die an den Seiten anderer Nodes angebracht werden können.
Trotz des Namens dieses Zeichentyps verwenden Schilder in der Regel nicht signlike,
sondern verwenden stattdessen den Zeichnungstyp `Nodebox`, um einen 3D-Effekt zu erzielen. Der Zeichentyp `signlike`
wird jedoch häufig von Leitern verwendet.

```lua
minetest.register_node("default:ladder_wood", {
    drawtype = "signlike",

    tiles = {"default_ladder_wood.png"},

    -- Erforderlich: Speichern der Drehung in param2
    paramtype2 = "wallmounted",

    selection_box = {
        type = "wallmounted",
    },
})
```


## Plantlike Nodes

<figure class="right_image">
    <img src="{{ page.root }}//static/drawtype_plantlike.png" alt="Plantlike Zeichnungstyp">
    <figcaption>
        Plantlike Zeichnungstyp
    </figcaption>
</figure>

Plantlike Nodes zeichnen ihre Kacheln in einem X-ähnlichen Muster.

```lua
minetest.register_node("default:papyrus", {
    drawtype = "plantlike",

    -- Nur eine Textur in Verwendung
    tiles = {"default_papyrus.png"},

    selection_box = {
        type = "fixed",
        fixed = {-6 / 16, -0.5, -6 / 16, 6 / 16, 0.5, 6 / 16},
    },
})
```

## Firelike Nodes

Firelike ist ähnlich zu plantlike, mit der Ausnahme, dass es so konmzipiert ist, dass es auch an Wänden und Decken haften kann.

<figure>
    <img src="{{ page.root }}//static/drawtype_firelike.png" alt="Firelike nodes">
    <figcaption>
        Firelike nodes
    </figcaption>
</figure>

```lua
minetest.register_node("meinemod:klinger", {
    drawtype = "firelike",

    -- Nur eine Textur in Verwendung
    tiles = { "mymod:klinger" },
})
```

## Mehr Zeichnungstypen

Dies ist keine vollständige Liste, es gibt noch weitere Arten:

* Fencelike
* Plantlike rooted - für Unterwasser-Pflanzen
* Raillike - für Schienen
* Torchlike - für 2D Wand/Boden/Decken-Nodes.
  Die Fackeln in Minetest Game verwenden zwei verschiedene Node-Definitionen von
  mesh nodes (default:torch und default:torch_wall).

Und lese immer [Lua API dokumentation](https://minetest.gitlab.io/minetest/nodes/#node-drawtypes)
für eine komplette Liste.
