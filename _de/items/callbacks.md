---
title: Node und Item Callbacks
layout: default
root: ../..
idx: 2.15
description: Erfahren sie über callbacks, Aktionen, und Ereignissen, einschließlich on_use, on_punch, on_place, on_rightclick
---

## Einleitung  <!-- omit in toc -->

Minetest verwendet hauptsächlich ein Callback-basiertes Modding-Design. Ein Callback ist eine Funktion,
die Sie an eine API übergeben und die aufgerufen wird, wenn ein Ereignis eintritt. Zum Beispiel können Sie
eine Funktion `on_punch` in einer Node-Definition angeben, die aufgerufen wird, wenn ein Spieler
einen Node anstößt. Es gibt auch globale Callbacks wie
`minetest.register_on_punchnode`, um Ereignisse für alle Nodes zu empfangen.

- [Item Callbacks](#item-callbacks)
  - [on_use](#on_use)
  - [on_place und on_secondary_use](#on_place-und-on_secondary_use)
  - [on_drop](#on_drop)
  - [after_use](#after_use)
- [item_place vs place_item](#item_place-vs-place_item)
- [Node Callbacks](#node-callbacks)
  - [Rechtsklick und Platzieren eines Nodes](#rechtsklick-und-platzieren-eines-nodes)
  - [Schlagen und abbauen](#schlagen-und-abbauen)
  - [...und mehr!](#und-mehr)


## Item Callbacks

Wenn ein Spieler einen Node, einen Handwerksgegenstand oder ein Werkzeug in seinem Inventar hat, kann er folgende Ereignisse auslösen
bestimmte Ereignisse:

| Callback         | Standard-Bindung              | Standard Wert                                |
|------------------|-------------------------------|----------------------------------------------|
| on_use           | links-click                   | nil                                          |
| on_place         | rechts-click auf einen Node   | `minetest.item_place`                        |
| on_secondary_use | rechts-click auf keinen Node  | `minetest.item_secondary_use` (does nothing) |
| on_drop          | Q                             | `minetest.item_drop`                         |
| after_use        | Abbauen eines Nodes           | nil                                          |


### on_use

Mit einem on_use Callback wird verhindert, dass das Item zum abbauen von Blöcken verwendet wird. Eine häufige
Verwendung des on_use Callback ist für Lebensmittel:

```lua
minetest.register_craftitem("meinemod:matschekuchen", {
    description = "Alien Matschekuchen",
    inventory_image = "meinessen_matschekuchen.png",
    on_use = minetest.item_eat(20),
})
```

Die Zahl, die an die Funktion minetest.item_eat übergeben wird, ist die Anzahl der Hitpoints,
Punkte, die durch den Verzehr dieser Nahrung geheilt werden. Jedes Herzsymbol, das der Spieler hat, ist
zwei Hitpoints wert. Ein Spieler kann in der Regel bis zu 10 Herzen haben, was gleichbedeutend ist mit
20 Hitpoints.

minetest.item_eat() ist eine Funktion, die eine Funktion zurückgibt und diese als on_use Callback. Das bedeutet, dass der obige Code dem hier entspricht:

```lua
minetest.register_craftitem("meinemod:matschekuchen", {
    description = "Alien Matschekuchen",
    inventory_image = "meinessen_matschekuchen.png",
    on_use = function(...)
        return minetest.do_item_eat(20, nil, ...)
    end,
})
```

Wenn man versteht, wie item_eat funktioniert, indem es einfach eine Funktion zurückgibt, ist es möglich, die Funktion so zu ändern, dass sie ein komplexeres Verhalten wie das Abspielen eines benutzerdefinierten Sounds ermöglicht.


### on_place und on_secondary_use

Der Unterschied zwischen `on_place` und `on_secondary_use` ist, dass `on_place` aufgerufen wird, 
wenn der Spieler auf einen Node zeigt und `on_secondary_use`, wenn der
Spieler dies nicht tut.

Beide Callbacks werden für alle Arten von Items aufgerufen. `on_place` ist standardmäßig auf die
Funktion `minetest.item_place`, die den Aufruf des `on_rightclick`
Callback des angezeigten Nodes aufruft oder das gehaltene Item platziert, wenn es ein Node ist.


### on_drop

on_drop wird aufgerufen, wenn der Spieler einen Gegenstand fallen lassen will, zum Beispiel mit
der Abwurftaste (Q) oder durch herausziehen aus dem Inventar. Es wird standardmäßig die Funktion
`minetest.item_drop` verwendet, die das Fallenlassen des Gegenstandes übernimmt.


### after_use

`after_use` wird beim abbauen eines Nodes aufgerufen und ermöglicht es Ihnen, dass die Art der Abnutzung
auf ein Werkzeug angewendet wird. Wenn after_use nicht existiert, dann ist es das gleiche wie:

```lua
after_use = function(itemstack, user, node, digparams)
    itemstack:add_wear(digparams.wear)
    return itemstack
end
```


## item_place vs place_item

Die API von Minetest enthält viele verschiedene integrierte Callback-Implementierungen, die Sie verwenden können. Diese Callbacks werden mit dem Elementtyp zuerst benannt, zum Beispiel,
`minetest.item_place` und `minetest.node_dig`. Einige Callback-Implementierungen werden
direkt verwendet, während einige Funktionen sind, die den Rückruf zurückgeben:

```lua
minetest.register_item("meinemod:beispiel", {
    on_place = minetest.item_place,
    on_use = minetest.item_eat(10),
})
```

Die API von Minetest enthält auch eingebaute Funktionen, die etwas _tun_. Diese sind
oft verwirrend ähnlich benannt wie die eingebauten Callback-Implementierungen
haben aber das Verb vorangestellt. Beispiele sind `minetest.place_item` und
`minetest.dig_node` - diese Funktionen ermöglichen das abbauen und Platzieren von Blöcken mit einem
ähnlichen Effekt wie Spieler.


## Node Callbacks

Wenn sich ein Node in einem Inventar befindet, verwendet er, wie oben beschrieben, Item Callback. Wenn
ein Node in der Welt platziert ist, verwendet er Node Callbacks. Es gibt eine ganze Menge von
Node Callbacks, zu viele, um sie in diesem Buch zu behandeln. Allerdings werden einige von ihnen
später in diesem Buch behandelt.

Einige der Callbacks beziehen sich auf Nodeoperationen wie das Platzieren und
Entfernen aus der Welt. Es ist wichtig zu beachten, dass Nodeoperationen-Callbacks
wie diese aus Leistungsgründen nicht von bulk changes - also solchen, die eine große Anzahl von
Blöcken auf einmal setzen - aufgerufen werden. Daher kann man sich nicht darauf verlassen, dass diese
Callbacks immer aufgerufen werden.


### Rechtsklick und Platzieren eines Nodes

Wenn der Benutzer mit der rechten Maustaste auf ein Item klickt, während er auf einen Node zeigt, wird der Callback
`on_place` des Items aufgerufen. Standardmäßig ist dieser auf `minetest.item_place` gesetzt.
Wenn der gezeigte Node einen `on_rightclick` Callback hat und sneak (shift) gehalten wird,
dann wird der `on_rightclick` Callback aufgerufen. Andernfalls wird `minetest.item_place`
den Node platzieren.

Das Platzieren eines Nodes ruft sowohl `on_construct` als auch `after_place_node` auf.
`on_construct` wird von jedem Node-Platzier-Ereignis aufgerufen, das nicht in Bulk war und es wird nur
die Position und den Wert des Nodes gegeben. `after_place_node` wird nur von node
place aufgerufen und hat daher mehr Informationen - wie den Placer und den Itemstack.

Es ist wichtig zu wissen, dass Spieler nicht die einzigen Objekte sind, die
Blöcke platzieren können; es ist üblich, dass auch Mobs und Mods Blöcke platzieren können. `Placer` kann ein Spieler, eine Entity oder nil sein.

```lua
minetest.register_node("mymod:mynode", {
    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        if clicker:is_player() then
            minetest.chat_send_player(clicker:get_player_name(), "Hallo Welt!")
        end
    end,
    on_construct = function(pos, node)
        local meta = minetest.get_meta(pos)
        meta:set_string("infotext", "Mein Block!")
    end,
    after_place_node = function(pos, placer, itemstack, pointed_thing)
        -- Überprüfen des Platzierers
        if placer and placer:is_player() then
            local meta = minetest.get_meta(pos)
            meta:set_string("owner", placer:get_player_name())
        end
    end,
})

```

### Schlagen und abbauen

Schlagen bedeutet, dass der Spieler kurz mit der linken Maustaste klickt. Wenn der benutze Gegenstand
einen `on_use` Callback hat, wird dieser aufgerufen. Andernfalls wird der `on_punch`
Callback auf dem angezeigten Node aufgerufen.

Wenn der Spieler versucht, einen Node abzubauen, wird der `on_dig` Callback auf dem Node aufgerufen.
Dieser ist standardmäßig auf `minetest.node_dig` eingestellt, der den Gebietsschutz prüft, das
das Werkzeug verschleißt, den Node entfernt und den `after_dig_node` Callback ausführt.


```lua
minetest.register_node("meinemod:meinnode", {
    on_punch = function(pos, node, puncher, pointed_thing)
        if puncher:is_player() then
            minetest.chat_send_player(puncher:get_player_name(), "Au!")
        end
    end,
})
```

### ...und mehr!

In der Lua-API-Referenz von Minetest finden Sie eine Liste aller Node-Callbacks und
weitere Informationen zu den oben genannten Rückrufen.
