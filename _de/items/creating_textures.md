---
title: Texturen erstellen
layout: default
root: ../..
idx: 2.2
description: Eine Einführung in die Erstellung von Texturen mit dem Editor Ihrer Wahl und eine Anleitung zu GIMP.
redirect_from: /de/chapters/creating_textures.html
---

## Einleitung <!-- omit in toc -->

Bei der Entwicklung für Minetest ist es sehr nützlich, Texturen zu erstellen und optimieren zu können.
Es gibt viele Techniken, die für die Arbeit an Pixel-Art-Texturen relevant sind
und das Verständnis dieser Techniken wird
die Qualität der von Ihnen erstellten Texturen erheblich verbessern.

Detaillierte Ansätze zur Erstellung guter Pixel-Art liegen außerhalb des Rahmens
dieses Buches, stattdessen werden nur die wichtigsten Grundtechniken
behandelt werden.
Es sind viele [gute Online-Tutorials](http://www.photonstorm.com/art/tutorials-art/16x16-pixel-art-tutorial)
zur Verfügung, die die Pixel-Art viel detaillierter behandeln.

- [Techniken](#techniken)
  - [Nutzung des Stiftes](#nutzung-des-stiftes)
  - [Kacheln](#kacheln)
  - [Transparenz](#transparenz)
  - [Farbpaletten](#farbpaletten)
- [Editore](#editore)
  - [MS Paint](#ms-paint)
  - [Aseprite / LibreSprite](#aseprite--libresprite)
  - [GIMP](#gimp)

## Techniken

### Nutzung des Stiftes

Das Stiftwerkzeug ist in den meisten Editoren verfügbar. Wenn es auf die kleinste Größe eingestellt ist,
können Sie damit einen Pixel nach dem anderen bearbeiten, ohne andere Teile des Bildes zu
ändern. Indem Sie die Pixel einzeln bearbeiten, schaffen Sie klare
und scharfe Texturen ohne ungewollte Unschärfen. Außerdem bietet es Ihnen ein hohes
Maß an Präzision und Kontrolle.

### Kacheln

Texturen, die für Blöcke verwendet werden, sollten in der Regel kachelfähig sein. Das bedeutet
wenn Sie mehrere Blöcke mit der gleichen Textur zusammen platzieren, sehen die Kanten
korrekt aus.

<!-- IMAGE NEEDED - cobblestone that tiles correctly -->

Wenn Sie die Kanten nicht richtig anpassen, ist das Ergebnis weit weniger ansprechend.

<!-- IMAGE NEEDED - node that doesn't tile correctly -->

### Transparenz

Transparenz ist wichtig bei der Erstellung von Texturen für fast alle Craftitems
und einige Nodes, wie z. B. Glas.
Nicht alle Editoren unterstützen Transparenz, also stellen Sie sicher, dass Sie einen
Editor, der für die Texturen, die Sie erstellen möchten, geeignet ist.

### Farbpaletten

Die Verwendung einer einheitlichen Farbpalette ist ein einfacher Weg, um Ihre Texturen viel besser aussehen zu lassen.
Es ist eine gute Idee, eine Palette mit einer begrenzten Anzahl von Farben zu verwenden, vielleicht höchstens 32.
Vorgefertigte Paletten finden Sie unter
[lospec.com](https://lospec.com/palette-list).

## Editore

### MS Paint

MS Paint ist ein einfacher Editor, der für einfaches Texturendesign nützlich sein kann; 
allerdings unterstützt er keine Transparenz.
Dies spielt normalerweise keine Rolle, wenn Sie Texturen für die Seiten von Nodes erstellen,
aber wenn Sie Transparenz in Ihren Texturen benötigen, sollten Sie einen
anderen Editor wählen.

### Aseprite / LibreSprite

[Aseprite](https://www.aseprite.org/) ist ein proprietärer Pixel-Art-Editor.
Er enthält standardmäßig eine Menge nützlicher Funktionen wie Farbpaletten und
Animationswerkzeuge.

[LibreSprite](https://libresprite.github.io/) ist ein Open-Source-Fork von Aseprite
bevor es proprietär wurde.

### GIMP

GIMP wird in der Minetest-Community häufig verwendet. Es hat eine ziemlich hohe
Lernkurve, da viele seiner Funktionen nicht sofort
offensichtlich sind.

Bei der Verwendung von GIMP kann das Bleistift-Werkzeug aus der Toolbox ausgewählt werden:

<figure>
    <img src="{{ page.root }}//static/pixel_art_gimp_pencil.png" alt="Pencil in GIMP">
</figure>

Es ist außerdem ratsam, das Kontrollkästchen "Harte Kanten" für das Radiergummi-Werkzeug zu aktivieren.
