---
title: Titelseite
layout: default
homepage: true
no_header: true
root: ..
idx: 0.1
---

<header>
    <h1>Minetest Modding-Buch</h1>

    <span>von <a href="https://rubenwardy.com" rel="author">rubenwardy</a></span>
    <span>Editiert von <a href="http://rc.minetest.tv/">Shara</a></span>
    <span>Übersetzt von von <a href="http://debiankaios.de/">debiankaios</a></span>
    <span>Korrekturgelessen von <a href="https://jojokorpi.ddns.net/">jjk1</a></span>
</header>

## Einleitung

Minetest verwendet Lua-Skripte, um Modding-Unterstützung zu bieten.
Dieses Buch soll Ihnen beibringen, wie Sie Ihre eigenen Mods erstellen können, beginnend mit den Grundlagen.
Jedes Kapitel konzentriert sich auf einen bestimmten Teil der API und wird Sie bald dazu bringen
eigene Mods zu erstellen.

Sowohl als [online lesbares Buch](https://rubenwardy.com/minetest_modding_book),
oder [in HTML form downloadbar](https://gitlab.com/rubenwardy/minetest_modding_book/-/releases).

### Feedback und Beiträge

Haben Sie einen Fehler bemerkt oder möchten Sie ein Feedback geben? Teilen Sie mir das bitte mit.

* Erstelle eine [GitLab Issue](https://gitlab.com/rubenwardy/minetest_modding_book/-/issues).
* Poste es im [Forum Topic](https://forum.minetest.net/viewtopic.php?f=14&t=10729).
* [Kontaktiere mich](https://rubenwardy.com/contact/).
* Lust Mitzumachen?
  [Read the README](https://gitlab.com/rubenwardy/minetest_modding_book/-/blob/master/README.md).
